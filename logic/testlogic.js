/**
 * Created by martin on 31/03/15.
 */

var isValid = require('./validation/validate'),
    ReportModel       = require('./model/report'),
    ReportController = require('./controller/report');



module.exports = function(err, line, stream){
    if(err) {
        throw new Error(err);
    }
    console.log(line);
    var matches = line.match(/([a-z0-9]*\=[a-z0-9]*\S*)/g);
    if(isValid(matches)) {
        var reportModelObject = ReportModel(matches);
        ReportController.addToUrlReport(reportModelObject);
        ReportController.addToDynoReport(reportModelObject);
    }
    stream.resume();

};
