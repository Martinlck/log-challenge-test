/**
 * Created by martin on 31/03/15.
 */


module.exports = function (matches) {
    var obj = {};
    matches.forEach(function (match) {
        var array = match.split("=");
        obj[array[0]] = array[1];
    });
    deleteUnusedData(obj);
    setPath(obj);
    setTotalResponseTime(obj);
    setKey(obj);
    return obj;
}

var deleteUnusedData = function (obj) {
    delete obj.fwd;
    delete obj.host;
    delete obj.status;
    delete obj.bytes;
    delete obj.at;
}

var setKey = function(obj) {
    var key = obj.method + "-"+ obj.path;
    obj.key = key;
}


var setTotalResponseTime = function(obj) {
    var connect = obj.connect.replace("ms", "");
    var service = obj.service.replace("ms", "");
    obj.responseTime = parseInt(connect) + parseInt(service);
    delete obj.connect;
    delete obj.service;
}

var setPath = function(obj) {
    var path = obj.path;
    var pathArray = path.split("/");
    pathArray.splice(3,1);
    var pathJoined = pathArray.join("/");
    obj.path = pathJoined;
}

